{ config, pkgs, ... }:

{
	# This value determines the NixOS release from which the default
	# settings for stateful data, like file locations and database versions
	# on your system were taken. It‘s perfectly fine and recommended to leave
	# this value at the release version of the first install of this system.
	# Before changing this value read the documentation for this option
	# (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
	system.stateVersion = "21.05"; # Did you read the comment?

	# Use the systemd-boot EFI boot loader.
	boot.loader = {
		systemd-boot.enable = true;
		efi.canTouchEfiVariables = true;
		timeout = 2;
	};

	# Configuration options for LUKS Device
	boot.initrd.luks.devices = {
		crypted = {
			device = "/dev/disk/by-partuuid/<PARTUUID of /dev/sda1>";
			header = "/dev/disk/by-partuuid/<PARTUUID of /dev/sdb2>";
			allowDiscards = true; # Used if primary device is a SSD
			preLVM = true;
		};
	};

	nix.gc.automatic = true;
	nix.gc.options = "--delete-older-than 8d";

	networking.hostName = "horo"; # Define your hostname.

	# Set your time zone.
	time.timeZone = "America/Sao_Paulo";

	# The global useDHCP flag is deprecated, therefore explicitly set to false here.
	# Per-interface useDHCP will be mandatory in the future, so this generated config
	# replicates the default behaviour.
	networking.useDHCP = false;
	networking.interfaces.enp2s0f1.useDHCP = true;
	networking.interfaces.wlp3s0.useDHCP = true;

	# Select internationalisation properties.
	i18n.defaultLocale = "en_US.UTF-8";
	console = {
		font = "Lat2-Terminus16";
		keyMap = "br-abnt2";
	};

	# Enable sound.
	sound.enable = true;
	hardware.pulseaudio.enable = true;

	# Enable touchpad support (enabled default in most desktopManager).
	services.xserver.libinput.enable = true;

	# Some programs need SUID wrappers, can be configured further or are
	# started in user sessions.
	# programs.mtr.enable = true;
	programs.gnupg.agent = {
		enable = true;
		enableSSHSupport = true;
	};
}
