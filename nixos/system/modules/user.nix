{ config, pkgs, ... }:

{
	programs.zsh.enable = true;
	users.defaultUserShell = pkgs.zsh;

	security.sudo.extraRules = [
		{ groups = [ "wheel" ]; commands = [ "NOPASSWD: ALL" ]; }
	];

	users.users.reberti = {
		uid = 1000;
		isNormalUser = true;
		hashedPassword = "$6$vxK/hBG3VY3GFoLn$rnWAPM4cjd9Lihe8v5r6lf7Z570bGYLYlrcUMY.l1NS92C/RWewOUZ71o02LQPWIbH2E6TaxUS3nrc45imhO60";
		shell = pkgs.zsh;

		extraGroups = [ "wheel" "docker" ]; # Enable ‘sudo’ for the user.
	};
}
