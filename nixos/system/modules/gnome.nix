
{ config, pkgs, ... }:

{
	# Enable the X11 windowing system.
	services.xserver.enable = true;


	# Enable the GNOME Desktop Environment.
	services.xserver.displayManager.gdm.enable = true;
	services.xserver.desktopManager.gnome.enable = true;
	
	# List packages installed in system profile. To search, run:
	# $ nix search wget
	environment.systemPackages = with pkgs; [
		# Theme
		yaru-theme
 
		# Extensions
		gnomeExtensions.just-perfection
		gnomeExtensions.gtk-title-bar
		gnomeExtensions.night-theme-switcher
		gnomeExtensions.tray-icons-reloaded
		gnomeExtensions.blur-my-shell
	];

	# GNOME Remove Packages
	environment.gnome.excludePackages = [ 
		pkgs.gnome-photos
		pkgs.gnome.gnome-music
		pkgs.gnome.gedit
		pkgs.epiphany
		pkgs.evince
		pkgs.gnome.gnome-characters
		pkgs.gnome.totem
		pkgs.gnome.tali
		pkgs.gnome.iagno
		pkgs.gnome.hitori
		pkgs.gnome.atomix
	];

	services.udev.packages = with pkgs; [ 
		gnome3.gnome-settings-daemon 
	];
}
