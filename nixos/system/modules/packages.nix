{ config, pkgs, ... }:

{
	# Non-FOSS software
	nixpkgs.config.allowUnfree = true;

	# List packages installed in system profile. To search, run:
	# $ nix search wget
	environment.systemPackages = with pkgs; [
		# Terminal
		wget
		sudo
		unrar
		p7zip

		# Graphical
		torbrowser
		firefox
		mpv
		zathura
		lollypop
		transmission-gtk

		# Gaming
		steam
		discord
		lutris
		wineWowPackages.stable

		# Devloptment
		gnumake
		gcc
		clang
		lldb
		gdb
		valgrind
		python3
		nodejs
	];

	virtualisation.docker.enable = true;
}

