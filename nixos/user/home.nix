{ config, lib, pkgs, ... }:

{
  home.stateVersion = "21.05";
  home.username = "reberti";
  home.homeDirectory = "/home/reberti";

  programs.home-manager.enable = true;
  programs.gpg.enable = true;

  imports = [
    ./modules/zsh.nix
    ./modules/git.nix
    ./modules/neovim.nix
    ./modules/doom-emacs.nix

    # Config Files
    ./config/wget.nix
    ./config/transmission.nix
  ];
}
