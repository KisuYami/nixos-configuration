{ config, lib, pkgs, ... }:

{
  programs.git = {
    enable = true;
    userName  = "KisuYami";
    userEmail = "rebertisoares@protonmail.com";

    aliases = {
      ammend = "commit --amend --all --no-edit";
    };

    signing = {
      key = "C5770CC89A6B1934";
      signByDefault = true;
    };
  };
}
