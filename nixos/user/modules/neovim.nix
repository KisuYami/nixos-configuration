{ config, lib, pkgs, ... }:

{
  programs.neovim = {
    enable = true;

    viAlias = true;
    vimdiffAlias = true;

    plugins = with pkgs.vimPlugins; [
      deoplete-nvim
      vim-sandwich
      papercolor-theme
    ];

    extraConfig = ''
      set number relativenumber
      set nobackup
      set noswapfile
      set autoindent
      set cursorline
      set laststatus=-1
      set clipboard=unnamedplus
      set background=dark
      colorscheme PaperColor
      syntax on
    '';

  };
}
