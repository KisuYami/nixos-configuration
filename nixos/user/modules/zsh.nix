{ config, lib, pkgs, ... }:

{
  programs.zsh = {
    enable = true;

    enableAutosuggestions = true;
    enableVteIntegration = true;

    defaultKeymap = "viins";

    initExtraFirst = ''
    POWERLEVEL9K_INSTANT_PROMPT=quiet
    if [[ -r "$HOME/.cache/p10k-instant-prompt-$USER.zsh" ]]; then
       source "$HOME/.cache/p10k-instant-prompt-$USER.zsh"
    fi
    source $HOME/.p10k.zsh
    '';
    # dotDir # Directory where the zsh configuration is located

    zplug = {
        enable = true;
        plugins = [
          { name = "romkatv/powerlevel10k"; tags = [ as:theme depth:1 ]; } # Installations with additional options. For the list of options, please refer to Zplug README.
          { name = "zsh-users/zsh-syntax-highlighting"; } # Installations with additional options. For the list of options, please refer to Zplug README.
        ];
    };

    shellAliases = {
      nix-config = "sh /home/reberti/Development/Git/nixos-configuration/scripts/nix-config.sh";
      q = "exit";
    };

    history = {
      size = 10000;
      path = "${config.xdg.dataHome}/zsh/history";
    };
  };
}
