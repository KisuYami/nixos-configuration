#!/bin/sh
##
# NIXOS Configuration
#
# @file
# @version 0.1
# list of arguments expected in the input
ansi()          { echo -e "\e[${1}m${*:2}\e[0m"; }
bold()          { ansi 1 "$@"; }
italic()        { ansi 3 "$@"; }

function usage {
    printf "Options:
\t$(bold '-H|--home') \t\t $(italic Rebuild home-manager config.)
\t$(bold '-U|--update') \t\t $(italic Update home-manager.)
\t$(bold '-s|--system') \t\t $(italic Rebuild system config.)
\t$(bold '-u|--user') <user_name> \t $(italic Change the user.)
\t$(bold '-p|--path') <path> \t $(italic Change configuration path.)
\t$(bold '-h|--help') \t\t $(italic Shows this message.)
"
}

if [ $# -eq 0 ]; then
    usage
    exit 1
fi

CONFIG_PATH="`dirname \"$0\"`/../nixos/"
CONFIG_USER=$USER

while [ ! -z "$1" ]; do
  case "$1" in
     --path|-p)
         shift
         CONFIG_PATH=$1
         ;;
     --system|-s)
	     sudo nixos-rebuild switch -I nixos-config=$CONFIG_PATH/system/configuration.nix
         if [ $CONFIG_USER == $USER ]
         then
             CONFIG_USER=root
         fi
         ;;

     --home|-H)
	     sudo -u $CONFIG_USER home-manager switch -f $CONFIG_PATH/user/home.nix
         ;;

     --user|-u)
         shift
         CONFIG_USER=$1
         ;;

     --update|-U)
	     sudo -u $CONFIG_USER nix-channel --update
         ;;

     --help|-h)
         usage
         exit 1
         ;;

     *)
        usage
        exit 1
        ;;
  esac
shift
done
